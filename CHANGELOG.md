# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.5] - 20201-01-19
fix: notifier release from work path message
ci: modified to avoid triggering pipelines in tags
fix: changed timeout strategy in CLI
fix: add Handle Error.vi to prevent modules from closing. Fixes #8.
fix: increased limit in query by using perpage setting. Closes issue #10.
fix: regex for filtering url with trailing slash. Closes issue #6.

## [0.2.2] - 2020-12-02
### Added
- Included a indicator to show the Application Version in the Main VI. (issue #3)

### Fixed
- Incorrect Regex for returning main url and groups/subgroups (issue #5)
- Merge error order in Get Projects Data (Gitlab API v4) - (issue #4)

## [0.2.1] - 2020-11-04
### Fixed
- Bug for extracting files with long paths, longer than Windows supports.

## [0.2.0] - 2020-11-04
### Added
- Gitlab REST API v4 option for using a provider.

## [0.1.0] - 2020-10-30
### Added
- Minimum working version
- CLI implemented through G-CLI.
- Automatically package downloading from UI when providers are already added.

### Added for new features.
### Changed for changes in existing functionality.
### Deprecated for soon-to-be removed features.
### Removed for now removed features.
### Fixed for any bug fixes.
### Security in case of vulnerabilities.
