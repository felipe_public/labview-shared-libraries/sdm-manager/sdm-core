# SDM - Source Code Dependency Manager

Main project for the source code dependency manager (SDM).
This application is used to download source code directly from git source code providers, such as Gitlab, Github, BitBucket, and others.

# Author
Felipe Pinheiro Silva

## Installation

Use the NI Package provided in the [releases page](../../releases)

### Application Dependencies
- Requires NI Package Manager
- Requires LabVIEW Runtime Engine 2020

## Usage

### New Project
```mermaid
graph LR;
id1[Open Application]-->id2[Add Provider]-->id3[Open Working Path]-->id4[Insert Project Information]-->id5[Load Packages and Install Them];
```
### Existing Project (sdm-package.json exists)
```mermaid
graph LR;
id1[Open Application]-->id2[Add Provider - if needed]-->id3[Open Working Path]-->id4[Wait Packages Instalation];
```
### Command Line Interface
`
$ g-cli sdm.exe -- -ProjectPath "PathToTheProjectRoot" -Providers "Comma Separated Providers" -Tokens "Comma Separated Tokens"
`

**Note: The command works fine in Docker Image (dockerhub felipefoz/sdm). In Windows the environment variable must be set in PATH**

### Supported Providers Interfaces
- Gitlab (GraphQL+REST): private token always needed.
- BitBucket (REST): private token needed for private projects.
- Gitlab-v4 (REST): private token needed for private projects.

## Contributing
Merge Requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
