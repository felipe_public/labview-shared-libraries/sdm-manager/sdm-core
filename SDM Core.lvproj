﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Documentation" Type="Folder"/>
		<Item Name="Libraries" Type="Folder">
			<Item Name="Dev" Type="Folder">
				<Item Name="CI-CD Tools.lvlib" Type="Library" URL="../lib/cicd-tools.lvlib/CI-CD Tools.lvlib"/>
				<Item Name="Gitlab Tools LabVIEW.lvlib" Type="Library" URL="../lib/gitlab-tools-labview.lvlib/Gitlab Tools LabVIEW.lvlib"/>
			</Item>
			<Item Name="JDP Science" Type="Folder">
				<Item Name="JDP Timestamp.lvlib" Type="Library" URL="../lib/jdp-science-common-utilities/Timestamp/JDP Timestamp.lvlib"/>
				<Item Name="JDP Utility.lvlib" Type="Library" URL="../lib/jdp-science-common-utilities/JDP Utility.lvlib"/>
				<Item Name="JSONtext.lvlib" Type="Library" URL="../lib/jsontext/JSONtext.lvlib"/>
			</Item>
			<Item Name="GCLI" Type="Folder">
				<Item Name="CLI.lvclass" Type="LVClass" URL="../vendor/g-cli/CLI Class/CLI.lvclass"/>
				<Item Name="Option Parser.lvlib" Type="Library" URL="../vendor/g-cli/Argument Parser/Option Parser.lvlib"/>
			</Item>
			<Item Name="Broker.lvlib" Type="Library" URL="../lib/broker.lvlib/Broker.lvlib"/>
			<Item Name="Listbox Handler.lvlib" Type="Library" URL="../lib/listbox-handler.lvlib/Listbox Handler.lvlib"/>
			<Item Name="Message Transport.lvlib" Type="Library" URL="../lib/message-transport.lvlib/Message Transport.lvlib"/>
			<Item Name="Object to JSON.lvlib" Type="Library" URL="../lib/object-to-json.lvlib/Object to JSON.lvlib"/>
			<Item Name="Subscription Interface.lvlib" Type="Library" URL="../lib/subscription-interface.lvlib/Subscription Interface.lvlib"/>
			<Item Name="Topic.lvlib" Type="Library" URL="../lib/message-transport.lvlib/Topic.lvlib"/>
			<Item Name="UI Reference.lvlib" Type="Library" URL="../lib/ui-reference.lvlib/UI Reference.lvlib"/>
		</Item>
		<Item Name="Source" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="API Interfaces" Type="Folder">
				<Item Name="API Interface.lvclass" Type="LVClass" URL="../src/API Interfaces/api-interface.lvclass/API Interface.lvclass"/>
				<Item Name="Gitlab-Graphql.lvclass" Type="LVClass" URL="../src/API Interfaces/gitlab-graphql/Gitlab-Graphql.lvclass"/>
				<Item Name="BitBucket-2.0.lvclass" Type="LVClass" URL="../src/API Interfaces/bitbucket-2.0/BitBucket-2.0.lvclass"/>
				<Item Name="Gitlab-v4.lvclass" Type="LVClass" URL="../src/API Interfaces/gitlab-v4/Gitlab-v4.lvclass"/>
			</Item>
			<Item Name="Classes" Type="Folder">
				<Item Name="SDM-Provider-Array.lvclass" Type="LVClass" URL="../src/sdm-provider-array.lvclass/SDM-Provider-Array.lvclass"/>
				<Item Name="SDM-Provider.lvclass" Type="LVClass" URL="../src/sdm-provider.lvclass/SDM-Provider.lvclass"/>
			</Item>
			<Item Name="SDM-Broker.lvlib" Type="Library" URL="../src/sdm-broker.lvlib/SDM-Broker.lvlib"/>
			<Item Name="SDM Typedefs.lvlib" Type="Library" URL="../src/sdm-typedefs.lvlib/SDM Typedefs.lvlib"/>
			<Item Name="SDM-ConfigurationDB.lvlib" Type="Library" URL="../src/sdm-configurationdb.lvlib/SDM-ConfigurationDB.lvlib"/>
			<Item Name="sdm-ui.lvlib" Type="Library" URL="../src/sdm-ui.lvlib/sdm-ui.lvlib"/>
			<Item Name="SDM-Packages.lvlib" Type="Library" URL="../src/sdm-packages.lvlib/SDM-Packages.lvlib"/>
			<Item Name="SDM-Shared-UI.lvlib" Type="Library" URL="../src/shared-ui.lvlib/SDM-Shared-UI.lvlib"/>
			<Item Name="SDM-Shared-Actor-Msgs.lvlib" Type="Library" URL="../src/sdm-shared-actor-msgs/SDM-Shared-Actor-Msgs.lvlib"/>
			<Item Name="SDM-Constants.lvlib" Type="Library" URL="../src/sdm-constants.lvlib/SDM-Constants.lvlib"/>
			<Item Name="Main.vi" Type="VI" URL="../src/Main.vi"/>
			<Item Name="Topics.ctl" Type="VI" URL="../src/Topics.ctl"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Static" Type="Folder">
				<Item Name="custom" Type="Folder">
					<Item Name="CaseSensitiveCaseStructure.llb" Type="Folder">
						<Item Name="CaseSensitiveCaseStructure Config.vi" Type="VI" URL="../test/static/custom/CaseSensitiveCaseStructure.llb/CaseSensitiveCaseStructure Config.vi"/>
						<Item Name="CaseSensitiveCaseStructure RD.vi" Type="VI" URL="../test/static/custom/CaseSensitiveCaseStructure.llb/CaseSensitiveCaseStructure RD.vi"/>
						<Item Name="CaseSensitiveCaseStructure.vi" Type="VI" URL="../test/static/custom/CaseSensitiveCaseStructure.llb/CaseSensitiveCaseStructure.vi"/>
					</Item>
					<Item Name="CheckLVLibUsage.llb" Type="Folder">
						<Item Name="CheckLVLibUsage Config.vi" Type="VI" URL="../test/static/custom/CheckLVLibUsage.llb/CheckLVLibUsage Config.vi"/>
						<Item Name="CheckLVLibUsage RD.vi" Type="VI" URL="../test/static/custom/CheckLVLibUsage.llb/CheckLVLibUsage RD.vi"/>
						<Item Name="CheckLVLibUsage.vi" Type="VI" URL="../test/static/custom/CheckLVLibUsage.llb/CheckLVLibUsage.vi"/>
					</Item>
					<Item Name="CheckWhitespaceInFPnames.llb" Type="Folder">
						<Item Name="CheckWhitespaceInFPnames Config.vi" Type="VI" URL="../test/static/custom/CheckWhitespaceInFPnames.llb/CheckWhitespaceInFPnames Config.vi"/>
						<Item Name="CheckWhitespaceInFPnames RD.vi" Type="VI" URL="../test/static/custom/CheckWhitespaceInFPnames.llb/CheckWhitespaceInFPnames RD.vi"/>
						<Item Name="CheckWhitespaceInFPnames.vi" Type="VI" URL="../test/static/custom/CheckWhitespaceInFPnames.llb/CheckWhitespaceInFPnames.vi"/>
					</Item>
					<Item Name="Constant Documentation.llb" Type="Folder">
						<Item Name="Constant Documentation Config.vi" Type="VI" URL="../test/static/custom/Constant Documentation.llb/Constant Documentation Config.vi"/>
						<Item Name="Constant Documentation RD.vi" Type="VI" URL="../test/static/custom/Constant Documentation.llb/Constant Documentation RD.vi"/>
						<Item Name="Constant Documentation.vi" Type="VI" URL="../../../../Documents/LabVIEW Data/VI Analyzer Tests/Constant Documentation.llb/Constant Documentation.vi"/>
					</Item>
					<Item Name="Open Actor Core Front Panel Set to True.llb" Type="Folder">
						<Item Name="Input Guaranteed False.vi" Type="VI" URL="../test/static/custom/Open Actor Core Front Panel Set to True.llb/Input Guaranteed False.vi"/>
						<Item Name="OAC Inherits from an Actor.vi" Type="VI" URL="../test/static/custom/Open Actor Core Front Panel Set to True.llb/OAC Inherits from an Actor.vi"/>
						<Item Name="Open Actor Core Front Panel Set to True Config.vi" Type="VI" URL="../test/static/custom/Open Actor Core Front Panel Set to True.llb/Open Actor Core Front Panel Set to True Config.vi"/>
						<Item Name="Open Actor Core Front Panel Set to True RD.vi" Type="VI" URL="../test/static/custom/Open Actor Core Front Panel Set to True.llb/Open Actor Core Front Panel Set to True RD.vi"/>
						<Item Name="Open Actor Core Front Panel Set to True.vi" Type="VI" URL="../test/static/custom/Open Actor Core Front Panel Set to True.llb/Open Actor Core Front Panel Set to True.vi"/>
					</Item>
					<Item Name="VI Analyzer User Dictionary.txt" Type="Document" URL="../test/static/custom/VI Analyzer User Dictionary.txt"/>
				</Item>
				<Item Name="VI Analyzer Complete.viancfg" Type="Document" URL="../test/static/VI Analyzer Complete.viancfg"/>
			</Item>
			<Item Name="Unit" Type="Folder">
				<Item Name="SDM-ConfigurationDB Unit Test.lvlib" Type="Library" URL="../test/unit/SDM-ConfigurationDB/SDM-ConfigurationDB Unit Test.lvlib"/>
				<Item Name="API Interfaces Unit Tests.lvlib" Type="Library" URL="../test/unit/API Interface Unit Tests/API Interfaces Unit Tests.lvlib"/>
				<Item Name="UT Caller.vi" Type="VI" URL="../test/unit/UT Caller.vi"/>
				<Item Name="Provider Class.lvlib" Type="Library" URL="../test/unit/Provider Class/Provider Class.lvlib"/>
			</Item>
		</Item>
		<Item Name="imgtest.vi" Type="VI" URL="../imgtest.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Add State(s) to Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Add State(s) to Queue__jki_lib_state_machine.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Assert Same or Descendant Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Same or Descendant Type.vim"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Caraya Interactive Menu.rtm" Type="Document" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/menu/Caraya Interactive Menu.rtm"/>
				<Item Name="Caraya.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/Caraya.lvlib"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Create File and Containing Folders.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create File and Containing Folders.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Empty Picture" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Empty Picture"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get File System Separator.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysinfo.llb/Get File System Separator.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVComboBoxStrsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVComboBoxStrsAndValuesArrayTypeDef.ctl"/>
				<Item Name="LVMapReplaceAction.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMapReplaceAction.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="NI_Unzip.lvlib" Type="Library" URL="/&lt;vilib&gt;/zip/NI_Unzip.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Parse State Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Parse State Queue__jki_lib_state_machine.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="Path to URL inner.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL inner.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Read Class Ancestry Without Loading.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Read Class Ancestry Without Loading.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Remove Duplicates From 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Remove Duplicates From 1D Array.vim"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="TRef Traverse for References.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse for References.vi"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="VIAnCtl Config Control Value.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Config Control Value.ctl"/>
				<Item Name="VIAnCtl Reconstruct Data.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Reconstruct Data.ctl"/>
				<Item Name="VIAnCtl Results.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Results.ctl"/>
				<Item Name="VIAnCtl ResultsData (index).ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl ResultsData (index).ctl"/>
				<Item Name="VIAnCtl ResultStatus.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl ResultStatus.ctl"/>
				<Item Name="VIAnCtl Test Config Data.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Test Config Data.ctl"/>
				<Item Name="VIAnCtl TestRanking.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl TestRanking.ctl"/>
				<Item Name="VIAnRes Parsed Results Data (by Test).ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Parsed Results Data (by Test).ctl"/>
				<Item Name="VIAnRes Parsed Results Data (by VI).ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Parsed Results Data (by VI).ctl"/>
				<Item Name="VIAnUtil Clear Specific Error.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Clear Specific Error.vi"/>
				<Item Name="VIAnUtil Has Diagram.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Has Diagram.vi"/>
				<Item Name="VIAnUtil Has Panel.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Has Panel.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="TCP Get Raw Net Object.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Get Raw Net Object.vi"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="NI_App_Builder_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/AppBuilder/AB_API_Simple/NI_App_Builder_API.lvlib"/>
				<Item Name="GetTargetBuildSpecs.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/GetTargetBuildSpecs.vi"/>
				<Item Name="GetTargetBuildSpecs (project reference).vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/GetTargetBuildSpecs (project reference).vi"/>
				<Item Name="AB_Generate_Error_Cluster.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/AB_Generate_Error_Cluster.vi"/>
				<Item Name="Invoke BuildTarget.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Invoke BuildTarget.vi"/>
				<Item Name="Debug Log.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Debug Log/Debug Log.lvlib"/>
				<Item Name="Assert Complex Numeric Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Complex Numeric Type.vim"/>
				<Item Name="Assert Real Floating-Point Numeric Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Real Floating-Point Numeric Type.vim"/>
				<Item Name="Assert Integer Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Integer Type.vim"/>
				<Item Name="Assert Fixed-Point Numeric Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Fixed-Point Numeric Type.vim"/>
				<Item Name="Assert Floating-Point Numeric Type.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/TypeAssert/Assert Floating-Point Numeric Type.vim"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (GEN-REF)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (GObj-REF)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (VI-REF)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (CTL-REF)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Bool)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (Bool)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AConditional Auto-Indexing Tunnel (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Physical Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Physical Units__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4APhysical Units__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4APhysical Units__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Physical Units from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Physical Units from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AType Descriptor__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AType Descriptor__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet TDEnum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet TDEnum from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AType Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AType Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Element TD from Array TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Header from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Header from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AType Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AType Descriptor Header__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Error Cluster__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Error Cluster__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFormat Variant Into String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFormat Variant Into String__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet TDEnum from Data__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Refnum Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Refnum Type Enum from Data__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARefnum Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARefnum Subtype Enum__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Refnum Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Refnum Type Enum from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Units__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AVariant to Header Info__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AVariant to Header Info__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AArray Size(s)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AArray Size(s)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Array Element TDEnum__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Strings from Enum__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Strings from Enum TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet PString__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Waveform Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Waveform Type Enum from Data__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AWaveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AWaveform Subtype Enum__ogtk.ctl"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AResolve Timestamp Format__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AResolve Timestamp Format__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ATrim Whitespace__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ATrim Whitespace__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ATrim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ATrim Whitespace (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ATrim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ATrim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AArray to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AArray to Array of VData__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Data Name__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Data Name from TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Last PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Last PString__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASet Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASet Data Name__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AGet Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AGet Variant Attributes__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ACluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ACluster to Array of VData__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASplit Cluster TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASplit Cluster TD__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AParse String with TDs__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AParse String with TDs__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFile Exists__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFile Exists__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFile Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFile Exists - Scalar__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFile Exists - Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFile Exists - Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ARemove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AList Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AList Directory Recursive__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AList Directory__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AList Directory__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - Traditional__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - File Names Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - Traditional - path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - File Names Array - path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ABuild Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ABuild Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASearch 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASearch 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ASort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ASort 2D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder Array2__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder Array2__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AReorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4ADelete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AFilter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path Extension__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path Extension__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path Extension - Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path Extension - Path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path Extension - String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path Extension - String__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path Extension - 1D Array of Paths__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path Extension - 1D Array of Paths__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path Extension - 1D Array of Strings__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path Extension - 1D Array of Strings__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CDB)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (CSG)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (DBL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (EXT)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Path)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (SGL)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (String)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U8)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U16)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U32)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty Array (Variant)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (I64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (U64)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AEmpty 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AValid Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AValid Path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AValid Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AValid Path - Traditional__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AValid Path - Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AValid Path - Array__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path - Arrays__ogtk.vi"/>
				<Item Name="FF154AA9FA361469346938CA7F095A4AStrip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/FF154AA9FA361469346938CA7F095A4AStrip Path - Traditional__ogtk.vi"/>
				<Item Name="NI_VIAnalyzerTKVIs.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/analyzer/NI_VIAnalyzerTKVIs.lvlib"/>
				<Item Name="VIAnUI Path Type Checker.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Path Type Checker.vi"/>
				<Item Name="VIAnUI Path Type.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Path Type.ctl"/>
				<Item Name="VIAnAPI Ref.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnAPI Ref.ctl"/>
				<Item Name="VIAnAPI Operation.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnAPI Operation.ctl"/>
				<Item Name="VIAnCtl Operation.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Operation.ctl"/>
				<Item Name="VIAnAPI FG Analysis Progress.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Analysis Progress.vi"/>
				<Item Name="VIAnAPI FG Refs.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/analyzerapi.llb/VIAnAPI FG Refs.vi"/>
				<Item Name="VIAnAPI Generate Error Cluster.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianAPIshared.llb/VIAnAPI Generate Error Cluster.vi"/>
				<Item Name="VIAnUI Tree Hierarchy Update.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Tree Hierarchy Update.vi"/>
				<Item Name="VIAnCtl TreeHierarchy.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl TreeHierarchy.ctl"/>
				<Item Name="VIAnUI FG Test Config.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Test Config.vi"/>
				<Item Name="VIAnUI FG Test Hierarchy.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Test Hierarchy.vi"/>
				<Item Name="VIAnUtil Recursive Test Search.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Recursive Test Search.vi"/>
				<Item Name="VIAnUtil Alpha Sort Array.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Alpha Sort Array.vi"/>
				<Item Name="VIAnCtl Exclusion Data.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Exclusion Data.ctl"/>
				<Item Name="VIAnUtil Check Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Check Tests.vi"/>
				<Item Name="VIAnUI Remove Dup Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Remove Dup Tests.vi"/>
				<Item Name="VIAnUI Check if LLB is Test.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check if LLB is Test.vi"/>
				<Item Name="VIAnUI Remove LLB from Filename.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Remove LLB from Filename.vi"/>
				<Item Name="VIAnUI All FP Strings.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI All FP Strings.vi"/>
				<Item Name="VIAnUI Get Test Rankings.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Get Test Rankings.vi"/>
				<Item Name="VIAnUtil Test Name to Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Test Name to Path.vi"/>
				<Item Name="VIAnUtil Test LLB to VI.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Test LLB to VI.vi"/>
				<Item Name="VIAnUtil Remove VI from Filename.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Remove VI from Filename.vi"/>
				<Item Name="VIAnUI Default Test Rankings.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Default Test Rankings.vi"/>
				<Item Name="VIAnUI Tree Hierarchy Single.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Tree Hierarchy Single.vi"/>
				<Item Name="VIAnUI Get User-Specified CFG Data.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Get User-Specified CFG Data.vi"/>
				<Item Name="VIAnUI Get Stored Rank.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Get Stored Rank.vi"/>
				<Item Name="VIAnUI Close New Item and Children.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Close New Item and Children.vi"/>
				<Item Name="VIAnAPI FG Test Config.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Test Config.vi"/>
				<Item Name="VIAnAPI Get Test CFG Values.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianAPIshared.llb/VIAnAPI Get Test CFG Values.vi"/>
				<Item Name="VIAnUI Set Global Ctls.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Set Global Ctls.vi"/>
				<Item Name="VIAnAPI FG Test Hierarchy.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Test Hierarchy.vi"/>
				<Item Name="VIAnAPI FG CFG Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG CFG Path.vi"/>
				<Item Name="VIAnAPI FG Passwords.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Passwords.vi"/>
				<Item Name="VIAnAPI FG Item.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Item.vi"/>
				<Item Name="VIAnUI Item Info.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Item Info.ctl"/>
				<Item Name="VIAnAPI FG Excluded Paths.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Excluded Paths.vi"/>
				<Item Name="VIAnUI Load Config File.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Load Config File.vi"/>
				<Item Name="VIAnUI FP Ctrl Refs.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI FP Ctrl Refs.ctl"/>
				<Item Name="VIAnCtl AnalyzeMode.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl AnalyzeMode.ctl"/>
				<Item Name="VIAnUI FG CFG Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG CFG Path.vi"/>
				<Item Name="VIAnUI Config Format Change Warning Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Config Format Change Warning Dialog.vi"/>
				<Item Name="VIAnUI Old CFG File Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Old CFG File Dialog.vi"/>
				<Item Name="VIAnUI Init Analyzer UI.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Init Analyzer UI.vi"/>
				<Item Name="VIAnUI Config Change Operation.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Config Change Operation.ctl"/>
				<Item Name="VIAnUI FG Menu Launch VI.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Menu Launch VI.vi"/>
				<Item Name="VIAnUtil Special Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Special Error Handler.vi"/>
				<Item Name="VIAnUI FG Excluded Paths.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Excluded Paths.vi"/>
				<Item Name="VIAnUI FG Star.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Star.vi"/>
				<Item Name="VIAnUI FG Page.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Page.vi"/>
				<Item Name="VIAnUI FG Passwords.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Passwords.vi"/>
				<Item Name="VIAnUI FG Check Excl Test.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Check Excl Test.vi"/>
				<Item Name="VIAnUI FG Check Excl VI.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Check Excl VI.vi"/>
				<Item Name="VIAnUI FG Test Tag.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Test Tag.vi"/>
				<Item Name="VIAnUI FG Item.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Item.vi"/>
				<Item Name="VIAnUI FG Mode.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Mode.vi"/>
				<Item Name="VIAnUI Initialize Items Tree with Project Items.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Initialize Items Tree with Project Items.vi"/>
				<Item Name="VIAnUI Init Project UI Components.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Init Project UI Components.vi"/>
				<Item Name="VIAnCtl Provider Global Data.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Provider Global Data.ctl"/>
				<Item Name="VIAnUI FG TargetRefs.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG TargetRefs.vi"/>
				<Item Name="VIAnUI FG Red X Index.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Red X Index.vi"/>
				<Item Name="VIAnUI FG Project Tree Glyphs.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Project Tree Glyphs.vi"/>
				<Item Name="VIAnUI Init Custom Glyphs.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Init Custom Glyphs.vi"/>
				<Item Name="VIAnUI FG Subpanel VI Ref.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Subpanel VI Ref.vi"/>
				<Item Name="VIAnUI First Config Change.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI First Config Change.vi"/>
				<Item Name="VIAnUI Check Stored CFG Password.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check Stored CFG Password.vi"/>
				<Item Name="VIAnUI Decrypt Passwords.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Decrypt Passwords.vi"/>
				<Item Name="VIAnUI Obtained Pwd Global.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Obtained Pwd Global.vi"/>
				<Item Name="VIAnUI Check Stored CFG Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check Stored CFG Path.vi"/>
				<Item Name="VIAnUI Version Specific Item Read.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Version Specific Item Read.vi"/>
				<Item Name="VIAnUI Item Info (ver1).ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Item Info (ver1).ctl"/>
				<Item Name="VIAnUI REPOP Item(NEW).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Item(NEW).vi"/>
				<Item Name="VIAnUI Remove Nonpresent Items.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Remove Nonpresent Items.vi"/>
				<Item Name="VIAnUI Check File Exists.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check File Exists.vi"/>
				<Item Name="VIAnUI Add Item - VI.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Add Item - VI.vi"/>
				<Item Name="VIAnUI Search for Item.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Search for Item.vi"/>
				<Item Name="VIAnUI Generate Dummy Tag.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Generate Dummy Tag.vi"/>
				<Item Name="VIAnUI Add Item - LLB or FolderNEW.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Add Item - LLB or FolderNEW.vi"/>
				<Item Name="VIAnUI Add Removed Children to Items.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Add Removed Children to Items.vi"/>
				<Item Name="VIAnUI Programmatically Expand Tree Item.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Programmatically Expand Tree Item.vi"/>
				<Item Name="VIAnUI Dynamically Populate Items Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Dynamically Populate Items Tree.vi"/>
				<Item Name="VIAnUI Check Excluded SCC Subfolders.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check Excluded SCC Subfolders.vi"/>
				<Item Name="VIAnUtil Get SCC Exclusion Folder Name.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Get SCC Exclusion Folder Name.vi"/>
				<Item Name="VIAnAPI Dynamically Populate Items Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianAPIshared.llb/VIAnAPI Dynamically Populate Items Tree.vi"/>
				<Item Name="VIAnUI Remove Child Item Sub.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Remove Child Item Sub.vi"/>
				<Item Name="VIAnUtil For Loop Progress Bar.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil For Loop Progress Bar.vi"/>
				<Item Name="VIAnUI Restore Old Glyph.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Restore Old Glyph.vi"/>
				<Item Name="VIAnUI SEARCH Item.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI SEARCH Item.vi"/>
				<Item Name="VIAnUI Gen All Rel Paths.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Gen All Rel Paths.vi"/>
				<Item Name="VIAnUI Reset Loaded Test Refs.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Reset Loaded Test Refs.vi"/>
				<Item Name="VIAnUI REPOP Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Tests.vi"/>
				<Item Name="VIAnUI Make Test Path Control Values Relative.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Make Test Path Control Values Relative.vi"/>
				<Item Name="VIAnUI Check Variant if Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Check Variant if Path.vi"/>
				<Item Name="VIAnUI Repopulate Config Values.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Repopulate Config Values.vi"/>
				<Item Name="Extract Basic Types Ctls.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Extract Basic Types Ctls.vi"/>
				<Item Name="Find control by label.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Find control by label.vi"/>
				<Item Name="VIAnUI Merge Init and CFG Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Merge Init and CFG Tests.vi"/>
				<Item Name="VIAnUI Fix User-Specified Paths from CFG.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Fix User-Specified Paths from CFG.vi"/>
				<Item Name="VIAnUI Test Glyph Updater.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Test Glyph Updater.vi"/>
				<Item Name="VIAnUI Get Parents and Children.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Get Parents and Children.vi"/>
				<Item Name="VIAnUI REPOP Excluded Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Excluded Tests.vi"/>
				<Item Name="VIAnUI REPOP Passwords.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Passwords.vi"/>
				<Item Name="VIAnUI Init UI from Loaded Proj CFG.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Init UI from Loaded Proj CFG.vi"/>
				<Item Name="VIAnUI Expand Test Categories in Exclusion Data.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Expand Test Categories in Exclusion Data.vi"/>
				<Item Name="Version From Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Version From Tree.vi"/>
				<Item Name="VIAnUI CFG File Info Global.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI CFG File Info Global.vi"/>
				<Item Name="NI_VIAn_XML Parsing.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/analyzer/XML Parsing/NI_VIAn_XML Parsing.lvlib"/>
				<Item Name="Config From Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Config From Tree.vi"/>
				<Item Name="NI_VIAnCFGDataStructure.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/analyzer/VIAnCFGDataStructure/NI_VIAnCFGDataStructure.lvlib"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Exclusion Data From Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Exclusion Data From Tree.vi"/>
				<Item Name="Item Info From Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Item Info From Tree.vi"/>
				<Item Name="Tree Config Data From Tree.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/Tree/Tree Config Data From Tree.vi"/>
				<Item Name="VIAnUI Test Base Path.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Test Base Path.ctl"/>
				<Item Name="VIAnUI Get Test Config Value from XML.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI Get Test Config Value from XML.vi"/>
				<Item Name="VIAnUI REPOP Item (viancfg).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Item (viancfg).vi"/>
				<Item Name="VIAnUI REPOP Tests (viancfg).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Tests (viancfg).vi"/>
				<Item Name="VIAnUI REPOP Excluded Tests (viancfg).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianuishared.llb/VIAnUI REPOP Excluded Tests (viancfg).vi"/>
				<Item Name="VIAnEng Get Selected Tests.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianengshared.llb/VIAnEng Get Selected Tests.vi"/>
				<Item Name="VIAnUI FG Exclusion Hierarchy.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnUI FG Exclusion Hierarchy.vi"/>
				<Item Name="VIAnEng Test LLB to VI Path.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianengshared.llb/VIAnEng Test LLB to VI Path.vi"/>
				<Item Name="Librarian Get Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian Get Info.vi"/>
				<Item Name="Librarian Path Location.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian Path Location.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="VIAnUtil Return Analysis App Info.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Return Analysis App Info.vi"/>
				<Item Name="VIAnCtl Analysis App Info.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerctrls.llb/VIAnCtl Analysis App Info.ctl"/>
				<Item Name="VIAnUtil CompareTargetClass.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil CompareTargetClass.vi"/>
				<Item Name="VIAnRes Master Results Parser.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Master Results Parser.vi"/>
				<Item Name="VIAnRes FG Test Config.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnRes FG Test Config.vi"/>
				<Item Name="VIAnRes FG Parsed Test Info.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnRes FG Parsed Test Info.vi"/>
				<Item Name="VIAnRes FG Raw Results.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnRes FG Raw Results.vi"/>
				<Item Name="VIAnRes Parse Results Data (by Test).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Parse Results Data (by Test).vi"/>
				<Item Name="VIAnRes All FP Strings.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes All FP Strings.vi"/>
				<Item Name="VIAnRes Parse Results Data (by VI).vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Parse Results Data (by VI).vi"/>
				<Item Name="VIAnRes Parse Errors.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes Parse Errors.vi"/>
				<Item Name="VIAnRes VI or Test and Error.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianresshared.llb/VIAnRes VI or Test and Error.ctl"/>
				<Item Name="VIAnAPI FG Exclusion List.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_vianFGs.llb/VIAnAPI FG Exclusion List.vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Constant Documentation Config Global.vi" Type="VI" URL="../test/static/custom/Constant Documentation.llb/Constant Documentation Config Global.vi"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="TCP_NoDelay_Windows.vi" Type="VI" URL="../vendor/g-cli/Dependencies/TCP_NoDelay_Windows.vi"/>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="wsock32.dll" Type="Document" URL="wsock32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="provcom_StringGlobals.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_StringGlobals.vi"/>
			<Item Name="Run Tests in Active Project.vi" Type="VI" URL="/C/Program Files (x86)/National Instruments/LabVIEW 2020/project/Caraya/Run Tests in Active Project.vi"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read.vi"/>
			<Item Name="LV Config Read Boolean.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read Boolean.vi"/>
			<Item Name="LV Config Read Pathlist.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read Pathlist.vi"/>
			<Item Name="LV Config Read Color.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read Color.vi"/>
			<Item Name="LV Config Read Numeric (I32).vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read Numeric (I32).vi"/>
			<Item Name="LV Config Write.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write.vi"/>
			<Item Name="LV Config Write Boolean.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write Boolean.vi"/>
			<Item Name="LV Config Write String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write String.vi"/>
			<Item Name="LV Config Write Pathlist.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write Pathlist.vi"/>
			<Item Name="LV Config Write Color.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write Color.vi"/>
			<Item Name="LV Config Write Numeric (I32).vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Write Numeric (I32).vi"/>
			<Item Name="VIAnUI Obtain CFG Password.vi" Type="VI" URL="/C/Program Files (x86)/National Instruments/LabVIEW 2020/project/_VI Analyzer/analyzerui.llb/VIAnUI Obtain CFG Password.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="sdm-executable" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{68383CF9-E777-4D21-BE1F-77E8C8C6D14F}</Property>
				<Property Name="App_INI_GUID" Type="Str">{89628B08-BEC2-4AC4-ABE0-908EA0E040D0}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{BC437650-F6ED-4753-B285-EED94A7FD171}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">sdm-executable</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../build</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{12478660-4E78-4C78-B496-70FB7E2A3F7D}</Property>
				<Property Name="Bld_version.build" Type="Int">73</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">6</Property>
				<Property Name="Destination[0].destName" Type="Str">sdm.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../build/sdm.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../build/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{4F47B09B-9463-455D-8D6C-91D2DE40B34A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Source/Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Source/SDM Typedefs.lvlib</Property>
				<Property Name="Source[10].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[10].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Source/SDM-Broker.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Source/SDM-ConfigurationDB.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Source/sdm-ui.lvlib</Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Source/SDM-Packages.lvlib</Property>
				<Property Name="Source[5].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Source/SDM-Shared-UI.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Source/SDM-Shared-Actor-Msgs.lvlib</Property>
				<Property Name="Source[7].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[7].type" Type="Str">Library</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Source/SDM-Constants.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Source/Topics.ctl</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">11</Property>
				<Property Name="TgtF_fileDescription" Type="Str">sdm-executable</Property>
				<Property Name="TgtF_internalName" Type="Str">sdm-executable</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 </Property>
				<Property Name="TgtF_productName" Type="Str">sdm-executable</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{6841D8B7-8137-4203-BEEE-1980BE03A2ED}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">sdm.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="sdm-pkg" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_addToFeed" Type="Bool">false</Property>
				<Property Name="NIPKG_allDependenciesToFeed" Type="Bool">false</Property>
				<Property Name="NIPKG_allDependenciesToSystemLink" Type="Bool">false</Property>
				<Property Name="NIPKG_certificates" Type="Bool">true</Property>
				<Property Name="NIPKG_createInstaller" Type="Bool">false</Property>
				<Property Name="NIPKG_feedLocation" Type="Path">../builds/NI_AB_PROJECTNAME/sdm-pkg/Feed</Property>
				<Property Name="NIPKG_feedLocation.Type" Type="Str">relativeToCommon</Property>
				<Property Name="NIPKG_installerArtifacts" Type="Str"></Property>
				<Property Name="NIPKG_installerBuiltBefore" Type="Bool">false</Property>
				<Property Name="NIPKG_installerDestination" Type="Path">../build</Property>
				<Property Name="NIPKG_installerDestination.Type" Type="Str">relativeToProject</Property>
				<Property Name="NIPKG_lastBuiltPackage" Type="Str">sdm-core_0.1.0-2_windows_all.nipkg</Property>
				<Property Name="NIPKG_license" Type="Ref"></Property>
				<Property Name="NIPKG_packageVersion" Type="Bool">false</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">Fix in CLI timeout functions, adding custom option.</Property>
				<Property Name="NIPKG_storeProduct" Type="Bool">true</Property>
				<Property Name="NIPKG_VisibleForRuntimeDeployment" Type="Bool">false</Property>
				<Property Name="PKG_actions.Count" Type="Int">0</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">true</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">true</Property>
				<Property Name="PKG_buildNumber" Type="Int">0</Property>
				<Property Name="PKG_buildSpecName" Type="Str">sdm-pkg</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">0</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">20.0.1.49153-0+f1</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">LabVIEW Runtime (32-bit)</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">ni-labview-2020-runtime-engine-x86</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Programming Environments</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">The LabVIEW Runtime is a software add-on that enables engineers to run executables on a nondevelopment machine.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str">Application for managing dependencies based upon source code from git platforms.</Property>
				<Property Name="PKG_destinations.Count" Type="Int">1</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{E85D055D-ECC8-44A8-A80F-FEF9D27CA658}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">SDM</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">Source Code Dependency Manager</Property>
				<Property Name="PKG_displayVersion" Type="Str">0.2.6</Property>
				<Property Name="PKG_feedDescription" Type="Str"></Property>
				<Property Name="PKG_feedName" Type="Str"></Property>
				<Property Name="PKG_homepage" Type="Str">http://felipekb.com</Property>
				<Property Name="PKG_hostname" Type="Str"></Property>
				<Property Name="PKG_maintainer" Type="Str">Felipe P. Silva &lt;felipefoz@gmail.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">../build</Property>
				<Property Name="PKG_output.Type" Type="Str">relativeToProject</Property>
				<Property Name="PKG_packageName" Type="Str">sdm-core</Property>
				<Property Name="PKG_publishToSystemLink" Type="Bool">false</Property>
				<Property Name="PKG_section" Type="Str">Application Software</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">1</Property>
				<Property Name="PKG_shortcuts[0].Destination" Type="Str">root_1</Property>
				<Property Name="PKG_shortcuts[0].Name" Type="Str">SDM</Property>
				<Property Name="PKG_shortcuts[0].Path" Type="Path"></Property>
				<Property Name="PKG_shortcuts[0].Target.Child" Type="Str">{6841D8B7-8137-4203-BEEE-1980BE03A2ED}</Property>
				<Property Name="PKG_shortcuts[0].Target.Destination" Type="Str">{E85D055D-ECC8-44A8-A80F-FEF9D27CA658}</Property>
				<Property Name="PKG_shortcuts[0].Target.Source" Type="Ref">/My Computer/Build Specifications/sdm-executable</Property>
				<Property Name="PKG_shortcuts[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{E85D055D-ECC8-44A8-A80F-FEF9D27CA658}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/sdm-executable</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">EXE Build</Property>
				<Property Name="PKG_synopsis" Type="Str">SDM</Property>
				<Property Name="PKG_version" Type="Str">0.2.6</Property>
			</Item>
		</Item>
	</Item>
</Project>
